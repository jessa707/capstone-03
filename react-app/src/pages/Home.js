import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import Footer from '../components/Footer'
import Chat from '../components/Chat'

export default function Home(){
	return(
		<>
		<Chat/>
			<Banner/>
        	<Highlights/>
        	<Footer/>
			
		</>
	)
}
