import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Login() {
  // Initializes the use of the properties from the UserProvider in App.js file
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false); // Define isActive state variable

  const retrieveUser = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        console.log(result);

        // Store the user details retrieved from the token into the global user state
        setUser({
          id: result._id,
          isAdmin: result.isAdmin,
        });
      });
  };

  function authenticate(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (typeof result.accessToken !== 'undefined') {
          localStorage.setItem('token', result.accessToken);

          retrieveUser(result.accessToken);

          Swal.fire({
            title: 'Login Successful!',
            icon: 'success',
            text: 'Welcome to JD Accessories',
          });
        } else {
          Swal.fire({
            title: 'Authentication Failed!',
            icon: 'error',
            text: 'Email and Password do not match',
          });
        }
      });
  }

  useEffect(() => {
    if (email !== '' && password !== '') {
      // Enables the submit button if the form data has been verified
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    user.id !== null && user.isAdmin === true ? (
      <Navigate to="/admin" />
    ) : user.id !== null && user.isAdmin === false ? (
      <Navigate to="/products" />
    ) : (
      <Container>
        <Row>
          <Col sm={6} md={12} lg={7}>
            <img className="img-fluid mt-5 mb-5 pt-5" height="500" width="600" src="/images/login.jpg" alt="first slide" />
          </Col>

          <Col sm={6} md={12} lg={5}>
            <Form className="block-low mt-5 mb-5 Auth-form-container" onSubmit={(event) => authenticate(event)}>
              <div className="Auth-form-content">
                <h3 className="Auth-form-title">Sign In</h3>
                <p className="text-center">
                  Not registered yet?{' '}
                  <Link className="link-warning text-center" to="/register">
                    Sign up
                  </Link>{' '}
                </p>
              </div>

              <Form.Group className="mt-3" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  value={email}
                  onChange={(event) => setEmail(event.target.value)}
                  required
                />
                <Form.Text className="text-muted text-white" variant="dark">
                  We'll never share your email with anyone else.
                </Form.Text>
              </Form.Group>

              <Form.Group className="mt-3 mb-4" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={(event) => setPassword(event.target.value)}
                  required
                />
              </Form.Group>

              {isActive ? (
                <Button className="button-login" type="submit" id="submitBtn">
                  Submit
                </Button>
              ) : (
                <Button className="button-login" id="submitBtn" disabled>
                  Submit
                </Button>
              )}
            </Form>
          </Col>
        </Row>
      </Container>
    )
  );
}
