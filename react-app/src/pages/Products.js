import ProductCard from '../components/ProductCard'
import Loading from '../components/Loading'
import {useEffect, useState, useContext} from 'react'
import ProductHeading from '../components/ProductHeading'
// import ProductView from '../components/ProductView'
// import courses_data from '../data/courses' <- WE DON'T NEED ANYMORE
import UserContext from "../UserContext";
import { Link, Navigate } from 'react-router-dom'


export default function Products(){
	const [products, setProducts] = useState([])
	const [isLoading, setIsLoading] = useState(false)
	const {user} = useContext(UserContext);

	useEffect((isLoading) => {
		// Sets the Loading state to true
		setIsLoading(true)

		fetch(`${process.env.REACT_APP_API_URL}/products/getAllActive`)
		.then(response => response.json())
		.then(result => {
			// console.log(result)
			setProducts(
				result.map(product => {
					return (
						<>
						<ProductCard key={product._id} product={product}/>
						</>
					)
				})
			)
			// Sets the loading state to false
			setIsLoading(false)
		})
	}, [])

	return(
			
				(isLoading) ?
					<Loading/>
			:
				<> 
					{
					(user.isAdmin === false) ?
					<ProductHeading/>
					:
					<h1 className="Auth-form-title mt-5 mb-5">Products</h1>
					}

					{products}
				</>
	)
}
