import { useContext, useEffect, useState } from 'react';
import { useParams, Navigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import UserContext from "../UserContext";
import {Link, useNavigate,} from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import Swal from "sweetalert2";

export default function UpdateUser() {


    const { productId } = useParams();
    const navigate = useNavigate();
    const {user} = useContext(UserContext);
    const [name, setName] = useState();
    const [description, setDescription] = useState();
    const [price, setPrice] = useState();

  
    
    useEffect(() => {


        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then((response) => response.json())
        .then(data => {
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);

            console.log(data);  
        })
    }, [productId]);


    function updateProduct(event){
        //prevents the page redirection via form submit
        event.preventDefault();

                fetch(`${process.env.REACT_APP_API_URL}/products/updateOneProduct/${productId}`, {
                    method: "PATCH",
                    headers:{
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${localStorage.getItem("token")}`
                    },
                    body: JSON.stringify({
                        name: name,
                        description: description,
                        price: price,
                        
                    })
                })
                .then(response => response.json())
                .then(data => {
                    console.log(data);

                    if(data){
                        //Clear input fields
                        setName("");
                        setDescription("");
                        setPrice("");
                        

                        Swal.fire({
                            title: "Completed",
                            icon: "success",
                            text: "Product successfully Updated!"
                        })

                        
                        navigate("/admin");
                    }
                    else{
                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        })
                    }
                })
            }


    return (
        
        (user.isAdmin === false) ?
        <Navigate to="*"/>
        :
        <Container>
          <Row>
            <Col md={{ span: 8, offset: 2 }}>
                <h3 className="Auth-form-title p-4">Update Product</h3>

                <Form className="md-4 col-lg-8 mx-auto" onSubmit={event => updateProduct(event)}>

                <Form.Group className="mb-3 mt-3" controlId="name">
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Product name"
                            required 
                            value={name}
                            onChange={event => setName(event.target.value)}
                        />
                    </Form.Group>
                    <Form.Group className="mb-3 mt-3" controlId="description">
                        <Form.Label>Description</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Description" 
                            required
                            value={description}
                            onChange={event => setDescription(event.target.value)}
                        />
                    </Form.Group>
                    <Form.Group className="mb-3 mt-3" controlId="price">
                        <Form.Label>Price</Form.Label>
                        <Form.Control 
                            type="number" 
                            placeholder="Price" 
                            required
                            value={price}
                            onChange={event => setPrice(event.target.value)}
                        />
                    </Form.Group>

                
                <Button id="update-product-btn" type="submit">Save Changes</Button>
                

                <Link to="/admin">
                <Button className="m-4 text-white" variant="warning" type="submit" id="submitBtn">
                    Back to Admin Dashboard
                </Button>
                </Link>
                </Form>
            </Col>
          </Row>
        </Container>
        
    )
   
}



     

