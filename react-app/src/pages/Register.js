import {Form, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Register() {
	// Activity
	const {user, setUser} = useContext(UserContext)
	const navigate = useNavigate()
	// Activity END


	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')

	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')

	// For determining if button is disabled or not
	const [isActive, setIsActive] = useState(false)

	
	function registerUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/check-email`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result === true) {
				Swal.fire({
					title: 'Oops!',
					icon: 'error',
					text: 'Email already exist!'
				})
			} else {
				// ACTIVITY HERE
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNumber,
						email:email,
						password: password1
					})
				})
				.then(response => response.json())
				.then(result => {
					console.log(result);
					
					setEmail('');
					setPassword1('');
					setPassword2('');
					setFirstName('');
					setLastName('');
					setMobileNumber('');

					if(result){
						Swal.fire({
							title: 'Register Successful!',
							icon: 'success',
							text: 'Salamat sa pag-rehistro!'
							})
						
						navigate('/login')

					} else {
						Swal.fire({	
							title: 'Registration Failed',
							icon: 'error',
							text: "Tropa, mukang mali ang iyong inilagay! :("
						})
					}		
				})
			}
		})


		

	}
	

	
	
	function authenticate(event){
		event.preventDefault(event)

		localStorage.setItem('email', email)

		setUser({
			email: localStorage.getItem('email')
		})

		setEmail('')

		navigate('/')
	}

	

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && mobileNumber.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			// Enables the submit button if the form data has been verified
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNumber, email, password1, password2])


	return (
		(user.id !==null) ?
			<Navigate to="/login"/>
		:
		<Container>
		  <Row>
           <Col md={{ span: 8, offset: 2 }}>
			<Form className="mt-5 mb-5 col-sm-12 col-md-12 col-lg-12 justify-content-center Auth-form-container" onSubmit={event => registerUser(event)}>
				<div className="Auth-form-content">
				  <h3 className="Auth-form-title p-2">Register</h3>
				</div>
				<Form.Group className="mt-4" controlId="firstName">
			        <Form.Label>First Name</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter First Name"
				            value={firstName} 
				            onChange={event => setFirstName(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group className="mt-3" controlId="lastName">
			        <Form.Label>Last Name</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter Last Name"
				            value={lastName} 
				            onChange={event => setLastName(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group className="mt-3" controlId="mobileNumber">
			        <Form.Label>Mobile Number</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter Mobile Number"
				            value={mobileNumber} 
				            onChange={event => setMobileNumber(event.target.value)}
				            required
				        />
			    </Form.Group>

			   	<Form.Group className="mt-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				            type="email" 
				            placeholder="Enter email"
				            value={email} 
				            onChange={event => setEmail(event.target.value)}
				            required
				        />
			        <Form.Text className="text-muted">
			            We'll never share your email with anyone else.
			        </Form.Text>
			    </Form.Group>

	            <Form.Group className="mt-3"  controlId="password1">
	                <Form.Label>Password</Form.Label>
	                <Form.Control 
		                type="password" 
		                placeholder="Password" 
		                value={password1} 
				        onChange={event => setPassword1(event.target.value)}
		                required
	                />
	            </Form.Group>

	            <Form.Group className="mt-3 mb-4"  controlId="password2">
	                <Form.Label>Verify Password</Form.Label>
	                <Form.Control 
		                type="password" 
		                placeholder="Verify Password" 
		                value={password2} 
				        onChange={event => setPassword2(event.target.value)}
		                required
	                />
	            </Form.Group>

	            {	isActive ? 
	            		<Button variant="warning" type="submit" id="submitBtn">
			            	Submit
			            </Button>
			            :
			            <Button variant="warning" id="submitBtn" disabled>
			            	Submit
			            </Button>
	            }
	            
			</Form>

			</Col>
		  </Row>
		</Container>
	)
}