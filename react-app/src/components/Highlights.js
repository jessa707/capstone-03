import { Row, Col, Card } from 'react-bootstrap'

export default function Highlights() {
  return (
    
    <Row>
      <Col xs={12} md={4} className="mt-1 p-3 mb-5">
        <Card className="cardHighlight text-light p-3 block" bg="dark">
          <Card.Body>
            <Card.Title>
              <h2>We Sell Computer Parts and Accessories</h2>
            </Card.Title>
            <img
              src="./images/we sell.jpg" // Replace with the actual image path
              alt="Image 1"
              className="highlight-image"
            />
            <Card.Text>
              Lorem cillum consequat ad. Consectetur enim sunt amet sit nulla dolor exercitation est pariatur aliquip minim. Commodo velit est in id anim deserunt ullamco sint aute amet. Adipisicing est Lorem aliquip anim occaecat consequat in magna nisi occaecat consequat et. Reprehenderit elit dolore sunt labore qui.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4} className="mt-1 p-3 mb-5">
        <Card className="cardHighlight text-dark p-3 block" bg="dark">
          <Card.Body>
            <Card.Title>
              <h2>Product Warranty (2-3 years)</h2>
            </Card.Title>
            <img
              src="./images/warranty.jpg" // Replace with the actual image path
              alt="Image 2"
              className="highlight-image"
            />
            <Card.Text>
              Ex Lorem cillum consequat ad. Consectetur enim sunt amet sit nulla dolor exercitation est pariatur aliquip minim. Commodo velit est in id anim deserunt ullamco sint aute amet. Adipisicing est Lorem aliquip anim occaecat consequat in magna nisi occaecat consequat et. Reprehenderit elit dolore sunt labore qui.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4} className="mt-1 p-3 mb-5">
        <Card className="cardHighlight text-light p-3 block" bg="dark">
          <Card.Body>
            <Card.Title>
              <h2>We Offer Home Service</h2>
            </Card.Title>
            <img
              src="./images/repair.jpg" 
              alt="Image 3"
              className="highlight-image"
            />
            <Card.Text>
              Minim nostrud dolore consequat ullamco minim aliqua tempor velit amet. Officia occaecat non cillum sit incididunt id pariatur. Mollit tempor laboris commodo anim mollit magna ea reprehenderit fugiat et reprehenderit tempor. Qui ea Lorem dolor in ad nisi anim. Culpa adipisicing enim et officia exercitation adipisicing.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  )
}
