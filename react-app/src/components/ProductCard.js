import {useState, useEffect} from 'react'
import {Card, Button} from 'react-bootstrap'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'




export default function ProductCard({product}){

  // Destructuring the props
  const {name, description, price, _id} = product

  // Initialize a 'count' state with a value of zero (0)
  const [count, setCount] = useState(0) 
  const [slots, setSlots] = useState(15)
  const [isOpen, setIsOpen] = useState(true)

  
  return (
    <div className="align-product">
  <Card className="justify-content-center col-lg-6 col-sm-6 col-xs-2" bg="dark block-low mt-4 mb-3 p-2">
    <div className="d-flex justify-content-center">
      <img
        className="img-fluid"
        src="./images/screenshots.gif"
        alt="Product Image"
        style={{ maxWidth: '100%', height: 'auto' }}
      />
    </div>
    <Card.Body>
      <Card.Title style={{ color: 'white' }}>{name}</Card.Title>
      <Card.Subtitle style={{ color: 'green' }}>Description:</Card.Subtitle>
      <Card.Text style={{ color: 'white' }}>{description}</Card.Text>
      <Card.Subtitle style={{ color: 'white' }}>Price:</Card.Subtitle>
      <Card.Text style={{ color: 'green' }}>PHP {price}</Card.Text>
      <Link className="btn btn-warning" to={`/products/${_id}`}>
        Details
      </Link>
    </Card.Body>
  </Card>
</div>


  );
}



