import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { MDBBtn, MDBContainer } from 'mdb-react-ui-kit';

export default function Banner() {
  return (
    <>
      <Row>
        <Col className="p-1">
          <div className="banner-content">
            <img
              className="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5 banner-image"
              height="500"
              width="100%"
              src="./images/landing page.jpg"
              alt="first slide"
            />
            <h1>Computer Accessories</h1>
            <p> We offer the best prices for your PC</p>
            <Link to="/login">
              <Button variant="warning">Build Now!</Button>
            </Link>
          </div>
        </Col>
      </Row>

      <h1 className="text-center p-5 animated-heading">Why Choose JD Accessories?</h1>
    </>
  );
}
