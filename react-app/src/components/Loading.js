import Spinner from 'react-bootstrap/Spinner'

export default function Loading(){
	return(
			<Spinner className="mt-4" animation="border" variant="success" />
		)
}