import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { Link, NavLink } from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'

export default function AppNavbar(){

	const {user} = useContext(UserContext)

	return (
			<Navbar className="color-nav" variant="dark" expand="lg" sticky="top">
			      <Container fluid>
			        <Navbar.Brand className="navbar-title"> 
			        <img height="40" width="75" src="/images/Logo.png" alt="Jd"/> Accessories
			        </Navbar.Brand>
			        <Navbar.Toggle aria-controls="navbarScroll" />
			        <Navbar.Collapse id="basic-navbar-nav">
			          <Nav
			            className="me-auto my-2 my-lg-0"
			            style={{ maxHeight: '200px' }}
			            navbarScroll
			          >
	
			          
			          {	
			          	(user.isAdmin) ?
			          	<>
			          	<Nav.Link as={NavLink} to="/admin" className="nav-hover navbar-font">Admin Dashboard</Nav.Link>
			          	</>
			          	:
			          	<>
			          	<Nav.Link className="nav-hover navbar-font" as={NavLink} to="/">Home</Nav.Link>
			          	<Nav.Link className="nav-hover navbar-font" as={NavLink} to="/products">Products</Nav.Link>
			          	</>
			          }

			          {	
			          	(user.id) ?
			          	<>
			          	
			          	<Nav.Link className="p-0" as={NavLink} to="/logout">
			          		<Button eventKey={2} className="nav-margin" variant="dark" type="submit">
			          			Log Out
			          		</Button>
			          	</Nav.Link>
			          	</>
			          	:
			          	<>
			          		<Nav.Link className="nav-hover navbar-font" as={NavLink} to="/login">Login</Nav.Link>
			          		<Nav.Link className="nav-hover navbar-font" as={NavLink} to="/register">Register</Nav.Link>
			          	</>
			          }
			          
			          </Nav>
			        </Navbar.Collapse>
			      </Container>
			    </Navbar>

		)

/*	return(
		<Navbar bg="light" expand="lg">
			<Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as={NavLink} to="/">Home</Nav.Link>
					<Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
					{	(user.id) ?
						<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
						:
						<>
							<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
							<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
						</>
					}
					
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)*/
}
